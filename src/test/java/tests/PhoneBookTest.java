package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.PhoneBook;
import model.User;

public class PhoneBookTest {

	private PhoneBook pb;
	private User user;
	
	@Before
	public void setUp() {
		pb = new PhoneBook();
		user = new User("Bob Charlson", "+46 111 222");
	}

	@Test
	public void testCreatePhoneBook() {
		assertNotNull(pb);
	}

	@Test
	public void testAddUser() {
		pb.addUser("Michael Jackson", "123 123 123");
		assertEquals(pb.getUsers().size(), 1);
	}
	
	@Test
	public void testaddUserWithExistingUser() {
		pb.addUser(user);
		assertEquals(pb.getUsers().size(), 1);
	}
	
	@Test
	public void testRemoveUser() {
		User user = new User("Ada", "123 123");
		pb.addUser(user);
		assertEquals(pb.getUsers().size(), 1);
		pb.removeUserWithName("Ada");
		assertEquals(pb.getUsers().size(), 0);
	}

}

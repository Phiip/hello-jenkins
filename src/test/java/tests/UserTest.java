package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.User;

public class UserTest {

	private User user;
	private User userWithNameAndNumber;

	@Before
	public void setUp() {
		user = new User();
		userWithNameAndNumber = new User("Bo", "111 222");
	}

	@Test
	public void testCreateUser() {
		assertNotNull(user);
	}

	@Test
	public void testCreateUserWithNameAndNumber() {
		User user = new User("Hercules", "000 111 222");
		assertNotNull(user);
	}

	@Test
	public void testSetAndGetNameAndPhoneNumber() {
		user.setName("Anna B");
		user.setPhonenumber("123 123");
		assertEquals("Anna B", user.getName());
		assertEquals("123 123", user.getPhonenumber());
	}

	@Test
	public void testGetCorrectString() {
		assertEquals("Bo, 111 222", userWithNameAndNumber.toString());
	}
	
	@Test
	public void testGetAndSetAddress() {
		user.setAddress("Street 1");
		assertEquals("Street 1", user.getAddress());
	}

}

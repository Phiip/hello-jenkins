package model;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {

	private List<User> users;

	public PhoneBook() {
		this.users = new ArrayList<>();
	}

	public void addUser(String name, String number) {
		User newUser = new User(name, number);
		users.add(newUser);
	}

	public List<User> getUsers() {
		return users;
	}

	public void addUser(User user) {
		users.add(user);
	}

	public void removeUserWithName(String name) {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getName().equals(name)) {
				users.remove(i);
			}
		}
	}

}

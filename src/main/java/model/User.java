package model;

public class User {

	private String name;
	private String phonenumber;
	private String address;

	public User(String name, String number) {
		this.name = name;
		this.phonenumber = number;
	}

	public User() {
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public void setPhonenumber(String newPhonenumber) {
		this.phonenumber = newPhonenumber;
	}

	public String getName() {
		return name;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	@Override
	public String toString() {
		return name + ", " + phonenumber;
	}

	public void setAddress(String newAddress) {
		this.address = newAddress;		
	}
	
	public String getAddress() {
		return address;
	}

}

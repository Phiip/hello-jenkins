package gui;

import java.util.List;

import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import model.PhoneBook;
import model.User;

public class MainScene {

	private BorderPane root;
	private PhoneBook phoneBook;
	
	private TextArea statusField;
	
	public MainScene(PhoneBook pb) {
		this.phoneBook = pb;
		root = new BorderPane();
		statusField = new TextArea();
		
		fillStatusField();
		
		root.setCenter(statusField);
		
	}

	private void fillStatusField() {
		List<User> users = phoneBook.getUsers();
		for (User u : users) {
			statusField.appendText(u.toString() + "\n");
		}
	}

	public Parent getRoot() {
		return root;
	}

}

package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.PhoneBook;
import model.User;

public class MainGui extends Application {

	private PhoneBook pb;
	
	public static void main(String[] args) {
		Application.launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Phonebook app");
		
		pb = new PhoneBook();
		setUpSomeUsers();
		
		MainScene mainScene = new MainScene(pb);
		primaryStage.setScene(new Scene(mainScene.getRoot()));
		primaryStage.show();
	}

	private void setUpSomeUsers() {
		User newUser = new User("Bo Charlson", "123 000");
		User secondNewUser = new User("Carl Davidson", "111 333");
		pb.addUser(newUser);
		pb.addUser(secondNewUser);
	}

}
